export default {
  coverageProvider: "v8",
  testEnvironment: "node",
  roots: ['<rootDir>/src'],
  preset: 'ts-jest'
};