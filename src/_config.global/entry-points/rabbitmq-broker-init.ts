import {connect, Connection} from "amqplib";
import {RabbitmqBroker} from "../../infrastructure/entry-points/RabbitmqBroker/RabbitmqBroker";

const {
  RABBITMQ_HOSTNAME,
  RABBITMQ_USER,
  RABBITMQ_PWD
} = process.env



const rabbitmqConnection = async (): Promise<Connection> =>  {
  const rabbitmqUrl = `amqp://${RABBITMQ_USER}:${RABBITMQ_PWD}@${RABBITMQ_HOSTNAME}:5672`

  const connection = await connect(rabbitmqUrl)
  console.log('-RabbitMq uri : ', rabbitmqUrl)
  return connection
}



export const rabbitmqBrokerInit = async () => new RabbitmqBroker(await rabbitmqConnection())
