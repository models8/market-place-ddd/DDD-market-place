import {Application, Router} from "express";
import bodyParser from "body-parser";
import helmet from "helmet";
import compression from "compression";
import cookieParser from "cookie-parser";
import {IConfigExpress} from "../../../_config.global/entry-points/expressjs-app-init";
import {IExpressApp} from "./IExpressApp";


export class ExpressApp implements IExpressApp{

  constructor(
    private config: IConfigExpress,
    private app : Application,
    public router : Router
  ) {}

  public middlewaresInit() {
    this.app.use(helmet())
    this.app.use(compression())
    this.app.use(bodyParser.urlencoded({extended:false}))
    this.app.use(bodyParser.json())
    this.app.use(cookieParser())
  }


  public routerInit() {
    this.app.use(`/${this.config.apiName}`,this.router)
  }

  public boot() {
    this.middlewaresInit();
    this.routerInit()
    const {port} = this.config
    this.app.listen(port, () => {
      console.log(`App listening on port ${port}!`);
    });
  }
}