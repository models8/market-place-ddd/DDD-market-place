import {Router} from "express";

export interface IExpressApp {

  router : Router
  middlewaresInit() : void
  routerInit() : void
  boot() : void
}