import {Channel} from "amqplib";

export interface IRabbitmqBroker {
  initExchange () : Promise<void>
  createChannel () : Promise<Channel>
}