import {IIntegrationEvent} from "../../../_definitions/IntegrationEvent/IIntegrationEvent";

export enum EnumIntegrationEvent {
  cart_updated = "cart_updated",
  cart_deleted = "cart_deleted",
  cart_validated = "cart_validated",
  cart_created = "cart_created",
  product_created = "product_created",
  product_deleted = "product_deleted",
  notification_sent = "notification_sent",
  orderCancelled = "orderCancelled"
}

/*-------- Cart Integration Events ------------*/
export type ICartUpdated_IntegrationEvent = IIntegrationEvent<EnumIntegrationEvent.cart_updated, {
  cartId : string,
  productList : Array< [string,number]>
}>

export type ICartCreated_IntegrationEvent = IIntegrationEvent<EnumIntegrationEvent.cart_created, {
  cartId : string
  productList : Array< [string,number]>
  cartOwner : string
  createdAt : Date
}>

export type ICartDeleted_IntegrationEvent = IIntegrationEvent<EnumIntegrationEvent.cart_deleted, {
  cartId : string
}>

export type ICartValidated_IntegrationEvent = IIntegrationEvent<EnumIntegrationEvent.cart_validated, {
  cartId : string
}>

/*-------- Product Integration Events ------------*/

export type IProductDeleteted_IntegrationEvent = IIntegrationEvent<EnumIntegrationEvent.product_deleted, {
  productId : string
}>


/*-------- Order Integration Events ------------*/

export type IOrderCancelled_IntegrationEvent = IIntegrationEvent<EnumIntegrationEvent.orderCancelled, {
  commandId : string
}>


