import {EnumIntegrationEvent} from "./EnumIntegrationEvent";


export const QueueNames = {
  ...EnumIntegrationEvent,
  cart_all : "cart_all",
  product_all : "product_all",
  notification_all : "notifications_all",
  order_all : "order_all"
}

export const QueuesPatterns  : Record<keyof typeof QueueNames , string> = {
  //Cart
  cart_all : "cart.#",
  cart_updated : "cart.updated",
  cart_deleted : "cart.deleted",
  cart_validated : "cart.validated",
  cart_created : "cart.created",
  //Product
  product_all : "product.#",
  product_created : "product.created",
  product_deleted : "product.deleted",
  //Notif
  notification_sent : "notifications.sent",
  notification_all : "notifications.#",
  //Order
  order_all : "order.#",
  orderCancelled : "order.cancelled",
}

export const integrationEventExchange = "integration_event_exchange"