import {Client} from "@eventstore/db-client/dist/Client";
import {FORWARDS, JSONEventData, JSONEventType, START} from "@eventstore/db-client";
import {IStreamSnapshot} from "./IStreamSnapshot";
import {Result, success} from "either-result";
import {AppendResult} from "@eventstore/db-client/dist/types";


export type CompiledStream<DocProps extends Record<string, any>> = {data:DocProps, revision:bigint, streamId: string }

export class StreamModel<DocProps extends Record<string, any>> {

  public readonly eventNameList: string[]


  constructor(
    private schemaProperties: string[],
    private eventStoreClient: Client,
    private modelName: string,
    eventNameList: string[] = []
  ) {
    this.eventNameList = [...new Set(eventNameList).values()]
  }

  get client() {
    return this.eventStoreClient
  }


  public async getCompiledStream<Event extends JSONEventType = JSONEventType>(
    cartId: string,
    snapshot?: IStreamSnapshot<DocProps>
  ): Promise<null | CompiledStream<DocProps>> {

    const streamId = this.buildStreamId(cartId)
    const streamResult = this.eventStoreClient.readStream<Event>(streamId, {
      fromRevision: snapshot ? snapshot.revision : START,
      direction: FORWARDS,
    })

    let data = {}

    let revision: bigint = BigInt(0)
    for await (const {event} of streamResult) {

      if (!event)
        continue
      else if (snapshot && event.revision === snapshot.revision) {
        data = snapshot.data
        continue
      }

      this.schemaProperties.forEach(property => {
        if (event.data[property])
          data[property] = event.data[property]
      })
      revision = event.revision
    }

    if (revision && Object.keys(data).length)
      return {data: data as DocProps, revision, streamId}
    return null
  }

  public async buildSnapshotFromScracth(streamId: string): Promise<Result<string, IStreamSnapshot<DocProps>>> {
    const dataResult = await this.getCompiledStream(streamId)
    if (!dataResult)
      return fail("Aucune data n'as été trouvée pour construire la snapshot")
    else
      return success({
        streamId: streamId,
        data: dataResult.data,
        revision: dataResult.revision
      })
  }


  public async deleteStreamHard(itemId: string): Promise<void> {
    const streamId = this.buildStreamId(itemId)
    await this.eventStoreClient.tombstoneStream(streamId)
  }

  public async deleteStreamSoft(itemId: string): Promise<void> {
    const streamId = this.buildStreamId(itemId)
    await this.eventStoreClient.deleteStream(streamId)
  }

  public appendToStream<E extends JSONEventData>(itemId: string, event: E): Promise<AppendResult> {
    const streamId = this.buildStreamId(itemId)
    return this.eventStoreClient.appendToStream(streamId, event)
  }

  private buildStreamId(id: string): string {
    return `${this.modelName}-${id}`
  }


}