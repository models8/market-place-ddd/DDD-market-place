
export interface IStreamSchemaProperties {
  properties : string[]
}

export class StreamSchema<Props extends Record<string, any>> implements IStreamSchemaProperties{

  properties : string[] = []
  
  constructor(
    private schema : Props
  ) {
    this.extractPropertiesFromModel()
  }


  private extractPropertiesFromModel (){
    this.checkSchemaValidity()
    this.properties.push(...Object.keys(this.schema))
    Object.freeze(this.properties)
  }

  private checkSchemaValidity () : void{
    if(!this.schema)
      throw "Vous devez fournir un schema différent de undefined"
    if(Object.prototype.toString.call(this.schema) !== "[object Object]")
      throw "Vous devez fournir un schema de type object"
  }

}
