export interface IDomainEvent<Type extends string = string,  Data extends Record<string, any> = Object> {
  type: Type
  data : Data
  metadata : {
    dateTimeOccurred: Date;
    [keyl:string] : any
  }
}