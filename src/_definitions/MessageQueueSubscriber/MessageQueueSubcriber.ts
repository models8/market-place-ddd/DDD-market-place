import {Channel} from "amqplib";
import {IIntegrationEvent} from "../IntegrationEvent/IIntegrationEvent";
import {IEventHanler} from "../IEventHanler";
import {IMessageQueueSubscriber} from "./IMessageQueueSubscriber";

export abstract class MessageQueueSubcriber implements IMessageQueueSubscriber {

  protected constructor(
    private channel: Channel,
  ) {
  }

  public async addSubscription<IntegrationEvent extends IIntegrationEvent>(queueName: string, handler: IEventHanler): Promise<void> {
    const result = await this.channel.consume(queueName, (msg) => {
      if (msg && !msg.content)
        return this.channel.nack(msg, false, false)
      else if (msg === null)
        return console.log(`WARNING : "${queueName}" queue deleted`)

      const integrationEvent: IntegrationEvent = JSON.parse(msg.content.toString())
      handler.handle(integrationEvent)
      if (result)
        return this.channel.ack(msg)
      else this.channel.nack(msg, false, true)
    })
  }

  public abstract listen(): Promise<void>

}