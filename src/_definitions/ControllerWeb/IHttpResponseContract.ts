
export interface IHttpResponseContract<data=unknown> {
  title?: string,
  message?: string,
  data?: data,
  code:number
}