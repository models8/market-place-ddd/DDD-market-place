
type ObjOrUndef = Record<string, any>|undefined

export interface IHttpRequestContract<
  body extends ObjOrUndef,
  params extends ObjOrUndef,
  queries extends ObjOrUndef,
  cookies extends ObjOrUndef>
{
  body : body
  params : params
  queries : queries
  cookies : cookies
}
