import { Request, Response} from "express";
import {IController} from "./IController";
import {Error} from "mongoose";
import {IHttpResponseContract} from "./IHttpResponseContract";


abstract class Controller implements IController{


  protected abstract processRequest(req: Request, res: Response): Promise<IHttpResponseContract>

  public async execute (req: Request, res: Response): Promise<void> {
    try {
      const {code,...body} = await this.processRequest(req, res)
      res.status(code).json(body)
    }
    catch(err) {
      this.errorHandler( res , err )
    }

  }

  //------------------Errors Cases --------------------

  protected errorHandler (res: Response, err: unknown){
    switch (err){
      default :
        this.unexpectedError(res,err)
    }
  }

  private badRequest (res: Response, message?: string): void {
    res.status(400).json({message : message ? message : 'Bad Request' })
  }

  private unauthorized (res: Response, message?: string): void {
    res.status(401).json({message : message ? message : 'Unauthorized' })
  }

  private forbidden (res: Response, message?: string) : void{
    res.status(403).json({message : message ? message : 'Forbidden' })
  }

  private notFound (res: Response, message?: string): void {
    res.status(404).json({message : message ? message : 'Not found' })
  }

  private conflict (res: Response, message?: string): void {
    res.status(409).json({message : message ? message : 'Conflict' })
  }

  private tooManyRequests (res: Response, message?: string) : void{
    res.status(429).json({message : message ? message :'Too many requests' })
  }

  private unexpectedError (res: Response,error: unknown) : void {
    //sendRedAlerte() !
    console.log(error instanceof Error ? error.message : error)
    res.status(500).json({message: "Une erreur serveur s'est produite"})
  }


}

export {Controller}