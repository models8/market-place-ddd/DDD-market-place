import {IEntityBuilder} from "./IEntityBuilder";
import {failure, Result, success} from "either-result";


abstract class EntityBuilder<Entity,EntityProps> implements IEntityBuilder<Entity,EntityProps>{

  abstract get instanceOf()
  protected abstract methodFactory(entityProps : EntityProps ) : Entity

  protected valityChecking(entityDto : EntityProps) : Result<string[], true> {
    return success(true)
  }

  public build(propsEntities : EntityProps[]): Result<string[], Entity[]>
  public build(propsEntity : EntityProps) : Result<string[], Entity>
  public build(props : EntityProps | EntityProps[]) {

    //if want build multiple entities
    if(Array.isArray(props)){
      const entities : Entity[] = []
      for(const entityDto of   props){
        const checkValidity = this.valityChecking(entityDto)
        if(checkValidity.isFailure())
          return failure(checkValidity.value)
        else
          entities.push(this.methodFactory(entityDto))
      }
      return (success(entities))
    }

    //if want build single entity
    const checkValidity = this.valityChecking(props)
    if(checkValidity.isFailure())
      return failure(checkValidity.value)
    else
      return success(this.methodFactory(props) )

  }



}

export {EntityBuilder}