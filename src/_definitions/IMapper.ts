
export interface IMapper<Entity, DocumentDto > {

  documentPropsToEntity(documentDto : DocumentDto) : Entity
  entityToDocumentProps(modelDb : Entity) : DocumentDto

}