import {Result} from "either-result";

export interface IUseCase<ResponseDto, RequestDto=undefined> {

  execute(requestDTO: RequestDto) : Promise<Result<string,ResponseDto>>

}