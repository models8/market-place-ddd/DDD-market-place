import {Entity} from "../Entity/Entity";
import {IDomainEvent} from "../DomainEvent/IDomainEvent";
import {IAggregateRoot} from "./IAggregateRoot";


export abstract class AggregateRoot<props> extends Entity<props> implements IAggregateRoot{

  private domainEvents : Map<string,IDomainEvent> = new Map()

  public addDomainEvent(eventItem : IDomainEvent) : void {
    this.domainEvents.set(eventItem.type, eventItem)
  }

  public removeDomainEvent(eventType: string) : void {
    this.domainEvents.delete(eventType)
  }

  public releaseEvents() : IDomainEvent[]{
    const events = [...this.domainEvents.values()]
    this.domainEvents.clear()
    return events
  }

}