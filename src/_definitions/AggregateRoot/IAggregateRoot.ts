import {IDomainEvent} from "../DomainEvent/IDomainEvent";
import {IEntity} from "../Entity/IEntity";


export interface IAggregateRoot extends IEntity{
  addDomainEvent(eventItem : IDomainEvent) : void
  removeDomainEvent(eventName: string) : void
  releaseEvents() : IDomainEvent[]
}
