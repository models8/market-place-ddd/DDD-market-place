

export abstract class ValueObject<Props extends Record<string, any>> {

  protected readonly props : Props

  protected constructor(props : Props) {
    this.props = Object.freeze(props)
  }

  public equals (vo?: ValueObject<Props>) : boolean {
    if (vo === null || vo === undefined) {
      return false;
    }
    else if (vo.props === undefined) {
      return false;
    }
    else {
      const thisKeys = Object.keys(this.props)
      const voKeys = Object.keys(vo.props)
      if(thisKeys.length !== voKeys.length)
        return false
      for (const key of thisKeys){
        if(vo[key] !== this.props[key])
          return false
      }
      return true
    }
  }

}