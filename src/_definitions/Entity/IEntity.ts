import ObjectID from "bson-objectid";

export interface IEntity {
  readonly _id : ObjectID
  equals (object : IEntity) : boolean
}