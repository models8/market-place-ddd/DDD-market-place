import ObjectID from "bson-objectid";
import {IEntity} from "./IEntity";

type EntityProps =  {
  _id? : ObjectID,
  [key: string] : any
}

export abstract class Entity<Props extends EntityProps> implements IEntity{

  static isEntity = (objet : unknown) : objet is IEntity => objet instanceof Entity

  get _id()  : ObjectID {
    return this.props._id
  }

  protected props : Props&{_id: ObjectID}

  constructor(props : Props) {

    if(props._id && !(props._id instanceof ObjectID))
      throw "'_id' provided in entity isn't instance of 'ObjectID'"

    if(props._id){
      this.props = {
        ...props,
        _id : props._id
      }}
    else{
      this.props = {
        ...props,
        _id : new ObjectID()
      }}
  }

  public equals (objet : Entity<Props>) : boolean {

    if(objet === null || objet == undefined)
      return false
    else if(this === objet)
      return true
    else if(!Entity.isEntity(objet))
      return false
    else
      return this._id.equals(objet._id)
  }


}
