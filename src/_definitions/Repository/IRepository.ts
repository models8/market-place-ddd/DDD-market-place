import {IEntity} from "../Entity/IEntity";
import ObjectID from "bson-objectid";
import {Result} from "either-result";


export interface IRepository<Entity extends IEntity> {
  create(entity: Entity): Promise<void>
  delete(entity: Entity): Promise<void>
  getById(id: ObjectID): Promise<Result<null, Entity>>
  update(id: string, newItem: Entity): Promise<Result<string, true>>
}