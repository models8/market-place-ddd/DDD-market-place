import {IDomainEvent} from "../DomainEvent/IDomainEvent";
import {IEventHanler} from "../IEventHanler";

export interface IEventDispatcher{
  addListener(eventName:string, eventHandler : IEventHanler) : void
  dispatch(events : IDomainEvent[]) : void
}