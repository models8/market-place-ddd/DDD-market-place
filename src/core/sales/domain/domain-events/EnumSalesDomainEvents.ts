import {IDomainEvent} from "../../../../_definitions/DomainEvent/IDomainEvent";
import {EnumCartEvents} from "../entities/Cart-Aggregate/EnumCartEvents";
import {EnumProductEvents} from "../entities/Product-Aggregate/EnumProductEvents";

export const EnumSalesDomainEvents = {
  ...EnumCartEvents,
  ...EnumProductEvents
}


/*---- Cart Domain Event -----*/

export type IProductAddedToCart_DomainEvent = IDomainEvent<EnumCartEvents.productAddedToCart, {
  cartId : string
  productIdAdded : string
  newProductList : Array<[string, number]>
}>

export type ICartValidated_DomainEvent = IDomainEvent<EnumCartEvents.cartValidated, {
  cartId : string
}>

export type ICartDeleted_DomainEvent = IDomainEvent<EnumCartEvents.cartDeleted, {
  cartId : string
}>


export type IProductRemovedFromCart_DomainEvent = IDomainEvent<EnumCartEvents.productRemovedFromCart, {
  cartId : string
  productIdRemoved : string
  newProductList : Array<[string, number]>
}>

/*---- Product Domain Event -----*/

export type IProductPriceUpdated_DomainEvent = IDomainEvent<EnumProductEvents.productPriceUpdated, {
  productId : string
  price : number
}>
