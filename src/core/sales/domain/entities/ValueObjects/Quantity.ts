import {ValueObject} from "../../../../../_definitions/ValueObject";
import {failure, Result, success} from "either-result";

interface IQuantituProps {
  value : number
}

export class Quantity extends ValueObject<IQuantituProps>  {

  get value() : number {
    return this.props.value
  }

  private constructor(props : IQuantituProps ) {
    super(props);
  }

  public static create(quantity : number) : Result<string, Quantity> {
    if (quantity < 1)
      return failure("You cannot have a quantity less than 1")
    else if (quantity > 50)
      return failure("You cannot have a quantity geater than 50")

    return success(new Quantity({value: quantity}))
  }

}