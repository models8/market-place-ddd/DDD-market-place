import {IProductEntity, IProductBuildProps, IProductEntityDto} from "./IProductEntity";
import {EntityBuilder} from "../../../../../_definitions/EntityBuilder/EntityBuilder";
import {AggregateRoot} from "../../../../../_definitions/AggregateRoot/AggregateRoot";

class ProductBuilder extends EntityBuilder<IProductEntity, IProductBuildProps> {


  get instanceOf() : typeof ProductEntity{
    return ProductEntity
  }

  protected methodFactory(entity: IProductBuildProps): IProductEntity {
    return new ProductEntity(entity)
  }

}

class ProductEntity extends AggregateRoot<IProductBuildProps> implements IProductEntity {

  constructor(props:IProductBuildProps){
    super(props);
  }

  public lean() : Required<IProductEntityDto> {
    return JSON.parse(JSON.stringify({...this, _id:this._id }))
  }


}

export {ProductBuilder}