import {EntityBuilder} from "../../../../../_definitions/EntityBuilder/EntityBuilder";
import {ICartEntity, ICartEntityDto, ICartBuildProps} from "./ICartEntity";
import {AggregateRoot} from "../../../../../_definitions/AggregateRoot/AggregateRoot";
import ObjectID from "bson-objectid";
import {
  EnumSalesDomainEvents,
  ICartDeleted_DomainEvent,
  IProductAddedToCart_DomainEvent
} from "../../domain-events/EnumSalesDomainEvents";
import {DomainEvent} from "../../../../../_definitions/DomainEvent/DomainEvent";
import {failure, Result, success} from "either-result";
import {Quantity} from "../ValueObjects/Quantity";

class CartBuilder extends EntityBuilder<ICartEntity, ICartBuildProps> {

  get instanceOf() : typeof CartEntity{
    return CartEntity
  }

  protected override valityChecking(entityDto) : Result<string[], true> {
    //Make verifications
    return success(true)
  }

  protected methodFactory(entityProps: ICartBuildProps): ICartEntity {
    return new CartEntity(entityProps)
  }

}

class CartEntity extends AggregateRoot<ICartBuildProps> implements ICartEntity {

  constructor(props : ICartBuildProps){
    super(props);
  }

  addProductToCart(productId: ObjectID, qty: Quantity): Result<string, ICartEntity> {
    if (this.props.productList.has(productId))
      return failure("This product already existe on Cart")

    this.props.productList.set(productId,qty)
    const domainEvent : IProductAddedToCart_DomainEvent = new DomainEvent(
      EnumSalesDomainEvents.productAddedToCart,
      {
        cartId : this._id.toHexString(),
        newProductList :this.productListArray,
        productIdAdded : productId.toHexString()
      })
    this.addDomainEvent(domainEvent)
    return success(this)
  }

  deleteCart () : void {
    //Chck if can delete
    //Maybe change state for delete ...
    const domainEvent : ICartDeleted_DomainEvent = new DomainEvent(
      EnumSalesDomainEvents.cartDeleted,
      {
        cartId : this._id.toHexString(),
      })
    this.addDomainEvent(domainEvent)
  }


  changeProductQty(productId: ObjectID, newQty: Quantity): Result<string, ICartEntity> {
    return "" as any;
  }

  removeProductFromCart(productId: ObjectID): Result<string, ICartEntity> {
    return "" as any;
  }

  validateCart(): Result<string,ICartEntity> {
    return "" as any;
  }

  public lean() : Required<ICartEntityDto> {
    return {
      ...this.props,
      _id : this._id.toHexString(),
      cartOwner : this.props.cartOwner.toHexString(),
      productList : this.productListArray
    }
  }

  private get productListArray (): Array<[string, number]> {
    return [...this.props.productList.entries()].map(entry => [
      entry[0].toHexString(),
      entry[1].value
    ])
  }

}

export {CartBuilder}