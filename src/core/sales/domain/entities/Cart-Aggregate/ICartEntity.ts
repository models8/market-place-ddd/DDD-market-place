import ObjectID from "bson-objectid";
import {IAggregateRoot} from "../../../../../_definitions/AggregateRoot/IAggregateRoot";
import {Quantity} from "../ValueObjects/Quantity";
import {Result} from "either-result";

export interface ICartEntityDto {
  _id : string
  productList : Array<[string, number]>
  cartOwner : string
  createdAt : Date
}

export interface ICartBuildProps {
  _id? : ObjectID
  productList : Map<ObjectID, Quantity>
  cartOwner : ObjectID
  createdAt : Date
}

export interface ICartEntity extends IAggregateRoot {
  addProductToCart(productId : ObjectID, qty : Quantity) : Result<string, ICartEntity>
  removeProductFromCart(productId : ObjectID) : Result<string, ICartEntity>
  changeProductQty(productId : ObjectID, newQty : Quantity) : Result<string, ICartEntity>
  validateCart() : Result<string, ICartEntity>
  deleteCart() : void
  lean() : Required<ICartEntityDto>
}