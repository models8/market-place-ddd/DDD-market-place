import {IRepository} from "../../../../../../_definitions/Repository/IRepository";
import {IProductEntity} from "../../../../domain/entities/Product-Aggregate/IProductEntity";

export interface IProductRepository extends IRepository<IProductEntity>{
  productExist(productId : string) : Promise<boolean>
  deleteById(productId : string) : Promise<void>
}