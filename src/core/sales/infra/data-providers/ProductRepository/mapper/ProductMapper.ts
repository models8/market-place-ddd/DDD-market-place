import {IEntityBuilder} from "../../../../../../_definitions/EntityBuilder/IEntityBuilder";
import {IProductMapper} from "./IProductMapper";
import {IProductEntity, IProductBuildProps} from "../../../../domain/entities/Product-Aggregate/IProductEntity";
import {IProductDocumentProps} from "../model/ProductModel";
import ObjectID from "bson-objectid";

export class ProductMapper implements IProductMapper {

  constructor(
    private cartbuilder : IEntityBuilder<IProductEntity, IProductBuildProps>
  ) {}

  entityToDocumentProps(entity: IProductEntity) : IProductDocumentProps {
    const productDto =  entity.lean()
    return {
      ...productDto,
      _id : new ObjectID(productDto._id)
    }
  }

  documentPropsToEntity(cartDocumentProps: IProductDocumentProps): IProductEntity {
    const entityResult = this.cartbuilder.build(cartDocumentProps)
    if(entityResult.isFailure())
      throw "Un problème de compatibilité est survenue : DocumentProps does't feet with DomainEntityProps"
    return entityResult.value
  }

}