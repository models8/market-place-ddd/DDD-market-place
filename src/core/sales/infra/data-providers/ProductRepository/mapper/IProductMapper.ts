import {IMapper} from "../../../../../../_definitions/IMapper";
import {IProductEntity, IProductBuildProps} from "../../../../domain/entities/Product-Aggregate/IProductEntity";

export type IProductMapper = IMapper<IProductEntity, IProductBuildProps>