import ObjectID from "bson-objectid";
import {Document, Model, Schema} from "mongoose";
import {MongoSalesConnection} from "../../../../_config.local/data-providers/init-sales-mongodb";

interface IProductDocumentProps {
  _id? : ObjectID
  priceTTC : number
  priceHT : number
  title : string
  description : string
  category : string
  subCategory : string
  available : boolean
}



type ProductDocument =  Document&IProductDocumentProps
//-------------Mongodb pair schema ----------------


const schema = new Schema({
  title :{type : String, required:'Vous devez entrer le titre'},
  priceTTC :{type : Number, required:'Vous devez entrer le prix TTC'},
  priceHT:{type : Number, required:'Vous devez entrer le prix HT'},
  description : {type : String, required:'Vous devez entrer la description'},
  category : {type : String, required:'Vous devez entrer la categorie'},
  subCategory : {type : String, required:'Vous devez entrer la sous-categorie'},
  available : {type : Boolean, required:'Vous devez entrer la disponibilitée'},
})

//-------------Create pairModel ----------------

// const ProductModel : Model<ProductDocument> = MongoSalesConnection.model('Products', schema)
const ProductModel : Model<ProductDocument> = MongoSalesConnection.model('Products', schema)

export {ProductModel, IProductDocumentProps}

