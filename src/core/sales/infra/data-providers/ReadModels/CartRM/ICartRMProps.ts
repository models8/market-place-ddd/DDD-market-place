export interface ICartRMProps {
  _id : string
  productList : Array<{
    productId : string,
    quantity: number
  }>
  cartOwner : string
  createdAt : Date
}
