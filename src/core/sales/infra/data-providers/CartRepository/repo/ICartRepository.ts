import {ICartEntity} from "../../../../domain/entities/Cart-Aggregate/ICartEntity";
import ObjectID from "bson-objectid";
import {Result} from "either-result";
import {EnumCartEvents} from "../../../../domain/entities/Cart-Aggregate/EnumCartEvents";

export interface ICartRepository{
  getCartById(cartId : ObjectID) : Promise<Result<null,ICartEntity>>
  getCartByOwner(ownerId : ObjectID) : Promise<Result<null,ICartEntity>>
  createCart(cartEntity : ICartEntity) : Promise<void>
  updateCart(cartEntity : ICartEntity, eventName: EnumCartEvents)
  deleteCart(cartEntity : ICartEntity) : Promise<void>
}