import {ICartRepository} from "./ICartRepository";
import {StreamModel} from "../../../../../../infrastructure/data-providers/EventStoreORM";
import {ICartDocumentProps} from "../model/CartModel";
import {ICartMapper} from "../mapper/ICartMapper";
import {ICartEntity} from "../../../../domain/entities/Cart-Aggregate/ICartEntity";
import {jsonEvent, JSONEventData} from "@eventstore/db-client";
import {
  ICartCreated,
  ICartEventTypes,
  ICartValidated,
  IProductAddedToCart, IProductQtyChanged,
  IProductRemovedFromCart
} from "../model/ICartEventTypes";
import ObjectID from "bson-objectid";
import {Result, success} from "either-result";
import {EnumCartEvents} from "../../../../domain/entities/Cart-Aggregate/EnumCartEvents";


export  class CartRepository implements ICartRepository {

  constructor(
    private cartModel : StreamModel<ICartDocumentProps>,
    private mapper : ICartMapper
  ) {}


  async createCart(cartEntity: ICartEntity): Promise<void> {
    const createCartEvent = jsonEvent<ICartCreated>({
      type: EnumCartEvents.cartCreated,
      data: this.mapper.entityToDocumentProps(cartEntity)
    })
    await this.cartModel.appendToStream(cartEntity._id.toHexString(),createCartEvent)
  }

  async updateCart(cartEntity: ICartEntity, eventName: EnumCartEvents) : Promise<void> {
    const documentProps = this.mapper.entityToDocumentProps(cartEntity)
    let event : JSONEventData<ICartEventTypes>
    switch (eventName){
      case EnumCartEvents.productAddedToCart:
        event  = jsonEvent<IProductAddedToCart>({
          type: EnumCartEvents.productAddedToCart,
          data: {
            productList : documentProps.productList
          }
        })
        await this.cartModel.appendToStream(documentProps._id,event)
        break
      case EnumCartEvents.productRemovedFromCart:
        event  = jsonEvent<IProductRemovedFromCart>({
          type: EnumCartEvents.productRemovedFromCart,
          data: {
            productList : documentProps.productList
          }
        })
        await this.cartModel.appendToStream(documentProps._id,event)
        break
      case EnumCartEvents.productQtyChanged:
        event  = jsonEvent<IProductQtyChanged>({
          type: EnumCartEvents.productQtyChanged,
          data: {
            productList : documentProps.productList
          }
        })
        await this.cartModel.appendToStream(documentProps._id,event)
        break
      case EnumCartEvents.cartValidated:
        event  = jsonEvent<ICartValidated>({
          type: EnumCartEvents.cartValidated,
          data: {
            ...documentProps
          }
        })
        await this.cartModel.appendToStream(documentProps._id,event)
        break
    }
  }

  async deleteCart(cartEntity: ICartEntity): Promise<void> {
    await this.cartModel.deleteStreamSoft(cartEntity._id.toHexString())
  }

  async getCartById(cartId: ObjectID): Promise<Result<null,ICartEntity>> {
    //GetSnapshot
    const cart = await this.cartModel.getCompiledStream(cartId.toHexString(),/*snapshot*/)
    if(!cart)
      return fail(null)
    const cartEntity = this.mapper.documentPropsToEntity(cart.data)
    //createNewSnapshot
    return success(cartEntity)
  }

  async getCartByOwner(ownerId: ObjectID): Promise<Result<null,ICartEntity>>  {
    const cart = await this.cartModel.getCompiledStream(ownerId.toHexString())
    if(!cart)
      return fail(null)
    const cartEntity = this.mapper.documentPropsToEntity(cart.data)
    return success(cartEntity)
  }



}
