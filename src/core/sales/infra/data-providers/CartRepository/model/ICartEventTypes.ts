import {JSONEventType} from "@eventstore/db-client";
import {ICartDocumentProps} from "./CartModel";
import {EnumCartEvents} from "../../../../domain/entities/Cart-Aggregate/EnumCartEvents";



export type ICartCreated = JSONEventType<
  EnumCartEvents.cartCreated,
  Required<ICartDocumentProps>
  >;

export type IProductAddedToCart = JSONEventType<
  EnumCartEvents.productAddedToCart,
  Pick<ICartDocumentProps,"productList">
  >;

export type IProductQtyChanged = JSONEventType<
  EnumCartEvents.productQtyChanged,
  Pick<ICartDocumentProps,"productList">
  >;

export type IProductRemovedFromCart = JSONEventType<
  EnumCartEvents.productRemovedFromCart,
  Pick<ICartDocumentProps,"productList">
  >;

export type ICartValidated = JSONEventType<
  EnumCartEvents.cartValidated,
  Required<ICartDocumentProps>
  >;

export type ICartDeleted = JSONEventType<
  EnumCartEvents.cartDeleted,
  Pick<ICartDocumentProps,"_id">
  >;

export type ICartEventTypes = ICartValidated | IProductQtyChanged |ICartCreated | IProductAddedToCart | IProductRemovedFromCart | ICartDeleted