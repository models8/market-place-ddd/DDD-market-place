import {IMapper} from "../../../../../../_definitions/IMapper";
import {ICartEntity} from "../../../../domain/entities/Cart-Aggregate/ICartEntity";
import {ICartDocumentProps} from "../model/CartModel";

export type ICartMapper = IMapper<ICartEntity, ICartDocumentProps>