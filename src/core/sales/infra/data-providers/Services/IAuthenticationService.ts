import {Result} from "either-result";
import ObjectID from "bson-objectid";

export interface IAuthenticationService {
  isAuth (token: string) : Promise<Result<false,ObjectID>>
}