import {IOrderCancelled_IntegrationEvent} from "../../../../../../../infrastructure/entry-points/RabbitmqBroker/EnumIntegrationEvent";
import {IEventHanler} from "../../../../../../../_definitions/IEventHanler";


export class OnOrderCancelled implements IEventHanler {

  constructor(
    //things what I need
  ) {
  }

  async handle(event: IOrderCancelled_IntegrationEvent): Promise<boolean> {
    //Execute event verifications and formatage...
    //Execute use case...
    //Return boolean success or fail
    return true
  }


}