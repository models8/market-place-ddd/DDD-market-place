import {IProductDeleteted_IntegrationEvent} from "../../../../../../../infrastructure/entry-points/RabbitmqBroker/EnumIntegrationEvent";
import {IEventHanler} from "../../../../../../../_definitions/IEventHanler";
import {IDeleteProduct} from "../../../../../application/use-cases/DeleteProduct-UC/IDeleteProduct";


export class OnProductDeleted implements IEventHanler {

  constructor(
    private deleteProduct : IDeleteProduct
  ) {
  }

  async handle(event: IProductDeleteted_IntegrationEvent): Promise<boolean> {
    //make something
    const result = await this.deleteProduct.execute({productId: event.data.productId})
    //make other thing
    if( result.isSuccess())
      return true
    else{
      console.log(result)
      return false
    }


  }


}