import {IEventHanler} from "../../../../../../../_definitions/IEventHanler";
import {Channel} from "amqplib";
import {integrationEventExchange, QueuesPatterns} from "../../../../../../../infrastructure/entry-points/RabbitmqBroker/IntegrationEventExchange";
import {IProductAddedToCart_DomainEvent} from "../../../../../domain/domain-events/EnumSalesDomainEvents";
import {IntegrationEvent} from "../../../../../../../_definitions/IntegrationEvent/IntegrationEvent";
import {EnumIntegrationEvent, ICartUpdated_IntegrationEvent} from "../../../../../../../infrastructure/entry-points/RabbitmqBroker/EnumIntegrationEvent";
import {ICartRM} from "../../../../data-providers/ReadModels/CartRM/CartRM";

export class OnProductAddedToCart implements IEventHanler {

  constructor(
    private brokerChannel : Channel,
    private cartReadModel : ICartRM
  ) {
  }

  async handle(event: IProductAddedToCart_DomainEvent): Promise<void> {
    await this.updateProjection(event.data)
    await this.sendIntegrationEvent(event.data)
    //Mark this event has complete

  }

  private async sendIntegrationEvent(data: IProductAddedToCart_DomainEvent["data"]) {
    const newEvent : ICartUpdated_IntegrationEvent = new IntegrationEvent(
      EnumIntegrationEvent.cart_updated,
      {
        cartId : data.cartId,
        productList : data.newProductList
      }
    )
    this.brokerChannel.publish(
      integrationEventExchange,
      QueuesPatterns.cart_updated, // "cart.updated"
      Buffer.from(JSON.stringify(newEvent)) )
  }

  private async updateProjection(data: IProductAddedToCart_DomainEvent["data"]) {
    await this.cartReadModel.updateOne(
      {_id: data.cartId},
      {productList : data.newProductList}
    )
  }


}