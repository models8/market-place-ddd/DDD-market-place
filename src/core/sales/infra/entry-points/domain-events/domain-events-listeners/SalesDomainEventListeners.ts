import {IEventDispatcher} from "../../../../../../_definitions/EventDispatcher/IEventDispatcher";
import {EnumCartEvents} from "../../../../domain/entities/Cart-Aggregate/EnumCartEvents";
import {DomainEventHandlersDI} from "../../../../_config.local/DI/DomainEventHandlersDI";


export class SalesDomainEventListeners {

  constructor(
    private salesDomainEventDispatcher : IEventDispatcher
  ) {
  }

  public listen(){

    this.salesDomainEventDispatcher.addListener(
      EnumCartEvents.productAddedToCart ,
      DomainEventHandlersDI.OnProductAddedToCartInstance
    )

    this.salesDomainEventDispatcher.addListener(
      EnumCartEvents.cartValidated ,
      DomainEventHandlersDI.OnCartValidatedInstance
    )

  }
}