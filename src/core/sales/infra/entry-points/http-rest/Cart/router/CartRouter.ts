import {Router} from "express";
import {IRouterHub} from "../../../../../../../_definitions/IRouterHub";
import {EnumRouterRootName} from "../../../../../../../infrastructure/entry-points/ExpressApp/EnumRouterRootName";
import {IController} from "../../../../../../../_definitions/ControllerWeb/IController";


export class CartRouter implements IRouterHub{

  private rootName: EnumRouterRootName = EnumRouterRootName.cart
  public router : Router = Router()

  constructor(
    private addProductToCartController : IController,
    private displayCartController : IController,
  ) {
    this.buildRouter();
  }

  private buildRouter(): void {
    this.router.post(`${this.rootName}/add-product/:cartId`, this.addProductToCartController.execute)
    this.router.get(`${this.rootName}/:cartId`, this.displayCartController.execute)
  }

}
