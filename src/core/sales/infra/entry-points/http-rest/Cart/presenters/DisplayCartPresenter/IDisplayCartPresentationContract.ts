export type IDisplayCartPresentationContract = {
  _id : string
  productList : Array<{
    productId : string
    quantity: number
    priceTTC : string
    priceHT : string
    category : string
    subCategory : string
    title : string
    description : string
    available : string
  }>
  totalTTC : string
  cartOwner : string
  createdAt : Date
}
