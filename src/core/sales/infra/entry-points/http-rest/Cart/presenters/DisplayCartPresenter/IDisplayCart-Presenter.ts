import {IPresenter} from "../../../../../../../../_definitions/IPresenter";
import {IDisplayCartResponseDto} from "../../../../../../application/use-cases/DisplayCart-UC/IDisplayCartResponseDto";
import {IDisplayCartPresentationContract} from "./IDisplayCartPresentationContract";


export type IDisplayCartPresenter = IPresenter<IDisplayCartResponseDto, IDisplayCartPresentationContract>