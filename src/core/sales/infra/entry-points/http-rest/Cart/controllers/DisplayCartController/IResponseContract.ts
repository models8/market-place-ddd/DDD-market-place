import {IHttpResponseContract} from "../../../../../../../../_definitions/ControllerWeb/IHttpResponseContract";
import {IDisplayCartPresentationContract} from "../../presenters/DisplayCartPresenter/IDisplayCartPresentationContract";

export type IResponseContract = Omit<IHttpResponseContract<IDisplayCartPresentationContract>, "message"|"title">
