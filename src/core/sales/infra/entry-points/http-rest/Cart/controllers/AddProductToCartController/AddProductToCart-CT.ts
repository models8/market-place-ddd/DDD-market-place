import {Controller} from "../../../../../../../../_definitions/ControllerWeb/Controller";
import {Request, Response} from "express";
import {IAddProductToCart} from "../../../../../../application/use-cases/AddProductToCart-UC/IAddProductToCart";
import {Result} from "either-result";
import {IRequestContract} from "./IRequestContract";
import {
  IAddProductToCartRequestDto
} from "../../../../../../application/use-cases/AddProductToCart-UC/IAddProductToCartRequestDto";
import {IResponseContract} from "./IResponseContract";

export class AddProductToCartCT extends Controller {

  constructor(
    private addProductToCart : IAddProductToCart,
  ) {
    super();
  }


  protected async processRequest(req: Request, res: Response): Promise<IResponseContract>  {
    const result = await this
      .checkContractValidity(req)
      .pipe(this.formatRequestDto)
      .pipeAsync(this.addProductToCart.execute)

    if(result.isFailure())
     throw result.value

    return {message: "Product added to cart",code:200}
  }

  private checkContractValidity (req: Request) : Result<string, IRequestContract>{
    return "" as any
  }

  private formatRequestDto (contract: IRequestContract) : Result<string, IAddProductToCartRequestDto>{
    return "" as any
  }

}