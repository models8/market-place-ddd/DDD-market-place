import {Controller} from "../../../../../../../../_definitions/ControllerWeb/Controller";
import {Request, Response} from "express";
import {Result} from "either-result";
import {IRequestContract} from "./IRequestContract";
import {IDisplayCart} from "../../../../../../application/use-cases/DisplayCart-UC/IDisplayCart";
import {IDisplayCartRequestDto} from "../../../../../../application/use-cases/DisplayCart-UC/IDisplayCartRequestDto";
import {IResponseContract} from "./IResponseContract";
import {IDisplayCartPresenter} from "../../presenters/DisplayCartPresenter/IDisplayCart-Presenter";
import {IDisplayCartPresentationContract} from "../../presenters/DisplayCartPresenter/IDisplayCartPresentationContract";

export class DisplayCartCT extends Controller {

  constructor(
    private displayCart : IDisplayCart,
    private displayCartPresenter : IDisplayCartPresenter
  ) {
    super();
  }


  protected async processRequest(req: Request, res: Response): Promise<IResponseContract>  {
    const result = await this
      .checkContractValidity(req)
      .pipe(this.formatRequestDto)
      .pipeAsync(this.displayCart.execute)

    if(result.isFailure())
     throw result.value

    const presentation : IDisplayCartPresentationContract = await this.displayCartPresenter.present(result.value)

    return {data: presentation ,code:200}
  }

  private checkContractValidity (req: Request) : Result<string, IRequestContract>{
    return "" as any
  }

  private formatRequestDto (contract: IRequestContract) : Result<string, IDisplayCartRequestDto>{
    return "" as any
  }

}