import {IDeleteCart } from "./IDeleteCart";
import {IDeleteCartResponseDto } from "./IDeleteCartResponseDto";
import {IDeleteCartRequestDto } from "./IDeleteCartRequestDto";
import {Result, success} from "either-result";

export class DeleteCart implements IDeleteCart {

  constructor() {
  }

  async execute(requestDTO?: IDeleteCartRequestDto) : Promise<Result<string, IDeleteCartResponseDto>>  {
    return success(true)
  }

}