import {IValidateCart } from "./IValidateCart";
import {IValidateCartResponseDto } from "./IValidateCartResponseDto";
import {IValidateCartRequestDto } from "./IValidateCartRequestDto";
import {Result, success} from "either-result";

export class ValidateCart implements IValidateCart {

  constructor() {
  }

  async execute(requestDTO: IValidateCartRequestDto) : Promise<Result<string, IValidateCartResponseDto>>  {
    return success(true)
  }

}