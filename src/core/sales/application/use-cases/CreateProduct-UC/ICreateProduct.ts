import {ICreateProductResponseDto } from "./ICreateProductResponseDto";
import {ICreateProductRequestDto } from "./ICreateProductRequestDto";
import {IUseCase} from "../../../../../_definitions/IUseCase";

export type ICreateProduct = IUseCase<ICreateProductResponseDto,ICreateProductRequestDto>