import {IRemoveProductFromCartResponseDto } from "./IRemoveProductFromCartResponseDto";
import {IRemoveProductFromCartRequestDto } from "./IRemoveProductFromCartRequestDto";
import {IUseCase} from "../../../../../_definitions/IUseCase";

export type IRemoveProductFromCart = IUseCase<IRemoveProductFromCartResponseDto,IRemoveProductFromCartRequestDto>