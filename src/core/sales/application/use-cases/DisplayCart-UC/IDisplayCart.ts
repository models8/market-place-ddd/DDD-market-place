import {IDisplayCartResponseDto } from "./IDisplayCartResponseDto";
import {IDisplayCartRequestDto } from "./IDisplayCartRequestDto";
import {IUseCase} from "../../../../../_definitions/IUseCase";

export type IDisplayCart = IUseCase<IDisplayCartResponseDto,IDisplayCartRequestDto>