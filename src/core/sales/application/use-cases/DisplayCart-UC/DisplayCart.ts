import {IDisplayCart } from "./IDisplayCart";
import {IDisplayCartResponseDto } from "./IDisplayCartResponseDto";
import {IDisplayCartRequestDto } from "./IDisplayCartRequestDto";
import {Result, success} from "either-result";
import {ICartRM} from "../../../infra/data-providers/ReadModels/CartRM/CartRM";
import {IAuthenticationService} from "../../../infra/data-providers/Services/IAuthenticationService";

export class DisplayCart implements IDisplayCart {

  constructor(
    private cartReadModel: ICartRM,
    private authService : IAuthenticationService
  ) {
  }

  async execute(requestDTO: IDisplayCartRequestDto) : Promise<Result<string, IDisplayCartResponseDto>>  {
   /*
   Check cartId and User:
   If cartId belongs to guest-session or authenticate user then Ok
   * */
    const [cart] : Omit<IDisplayCartResponseDto, "totalTTC">[] = await this.cartReadModel.aggregate(/* Aggregates CARTRM + PRODUCTS */)

    const responseDto : IDisplayCartResponseDto = {
      ...cart,
      totalTTC : cart.productList.reduce((acc,curr) =>  acc + curr.priceTTC,0 )
    }

    return success(responseDto)
  }

}