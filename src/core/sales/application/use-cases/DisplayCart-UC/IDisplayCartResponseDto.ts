export type IDisplayCartResponseDto = {
  _id : string
  productList : Array<{
    quantity: number
    priceTTC : number
    priceHT : number
    productId : string
    category : string
    subCategory : string
    title : string
    description : string
    available : boolean
  }>
  totalTTC : number
  cartOwner : string
  createdAt : Date
}