import {IDeleteProductResponseDto } from "./IDeleteProductResponseDto";
import {IDeleteProductRequestDto } from "./IDeleteProductRequestDto";
import {IUseCase} from "../../../../../_definitions/IUseCase";

export type IDeleteProduct = IUseCase<IDeleteProductResponseDto,IDeleteProductRequestDto>