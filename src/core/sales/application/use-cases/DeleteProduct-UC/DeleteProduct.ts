import {IDeleteProduct } from "./IDeleteProduct";
import {IDeleteProductResponseDto } from "./IDeleteProductResponseDto";
import {IDeleteProductRequestDto } from "./IDeleteProductRequestDto";
import {Result, success} from "either-result";

export class DeleteProduct implements IDeleteProduct {

  constructor() {
  }

  async execute(requestDTO?: IDeleteProductRequestDto) : Promise<Result<string, IDeleteProductResponseDto>>  {
    return success(true)
  }

}