import {IUseCase} from "../../../../../_definitions/IUseCase";
import {IAddProductToCartResponseDto } from "./IAddProductToCartResponseDto";
import {IAddProductToCartRequestDto } from "./IAddProductToCartRequestDto";

export type IAddProductToCart = IUseCase<IAddProductToCartResponseDto,IAddProductToCartRequestDto>