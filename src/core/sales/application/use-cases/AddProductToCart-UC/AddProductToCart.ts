import {IAddProductToCart} from "./IAddProductToCart";
import {IAddProductToCartResponseDto} from "./IAddProductToCartResponseDto";
import {IAddProductToCartRequestDto} from "./IAddProductToCartRequestDto";
import {failure, Result, success} from "either-result";
import {IEntityBuilder} from "../../../../../_definitions/EntityBuilder/IEntityBuilder";
import {ICartEntity, ICartBuildProps} from "../../../domain/entities/Cart-Aggregate/ICartEntity";
import {ICartRepository} from "../../../infra/data-providers/CartRepository/repo/ICartRepository";
import {IAuthenticationService} from "../../../infra/data-providers/Services/IAuthenticationService";
import ObjectID from "bson-objectid";
import {IProductRepository} from "../../../infra/data-providers/ProductRepository/repo/IProductRepository";
import {IEventDispatcher} from "../../../../../_definitions/EventDispatcher/IEventDispatcher";
import {Quantity} from "../../../domain/entities/ValueObjects/Quantity";

export class AddProductToCart implements IAddProductToCart {

  constructor(
    private cartbuilder : IEntityBuilder<ICartEntity, ICartBuildProps>,
    private cartRepository: ICartRepository,
    private productRepository: IProductRepository,
    private authService : IAuthenticationService,
    private eventDispatcher : IEventDispatcher
  ) {
  }

  async execute(requestDTO: IAddProductToCartRequestDto) : Promise<Result<string, IAddProductToCartResponseDto>>  {
    let ownerId : ObjectID
    if(requestDTO.authToken ){
      const authCheck =  await this.authService.isAuth(requestDTO.authToken )
      if(authCheck.isFailure())
        fail("you should be authenticate")
      ownerId = authCheck.value
    }
    else if(requestDTO.sessionId){
      const sessionId : any = requestDTO.sessionId //getSession() From DB session
      if(sessionId.isFailure())
        throw "BLABLALBAL"
      ownerId = sessionId.value
    }
    else
      ownerId = await this.createSession()


    const cart = await this.getCart(ownerId)
    const productExist = await this.productRepository.productExist(requestDTO.productID)
    if(!productExist)
      return failure("Commercial Error : Product doesn't exist")
    const qtyResult = Quantity.create(requestDTO.qty)
    if(qtyResult.isFailure())
      return failure(qtyResult.value)

    cart.addProductToCart(
      new ObjectID(requestDTO.productID),
      qtyResult.value
    )

    await this.cartRepository.createCart(cart)
    //Outboxpattern or EventStore subscription
    this.eventDispatcher.dispatch(cart.releaseEvents())

    return success(true)
  }


  private async getCart(ownerId: ObjectID) : Promise<ICartEntity> {
    const userCartResult : Result<any, ICartEntity> = await this.cartRepository.getCartByOwner(ownerId)
    return userCartResult.isFailure() ?
      this.createCart(ownerId) :
      userCartResult.value
  }

  private async createSession() : Promise<ObjectID>{
    return "" as any
  }

  private createCart(ownerId: ObjectID) : ICartEntity {
    return "" as any
  }


}