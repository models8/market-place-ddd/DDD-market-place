import {IUpdateProduct } from "./IUpdateProduct";
import {IUpdateProductResponseDto } from "./IUpdateProductResponseDto";
import {IUpdateProductRequestDto } from "./IUpdateProductRequestDto";
import {Result, success} from "either-result";

export class UpdateProduct implements IUpdateProduct {

  constructor() {
  }

  async execute(requestDTO?: IUpdateProductRequestDto) : Promise<Result<string, IUpdateProductResponseDto>>  {
    return success(true)
  }

}