import {IUpdateProductResponseDto } from "./IUpdateProductResponseDto";
import {IUpdateProductRequestDto } from "./IUpdateProductRequestDto";
import {IUseCase} from "../../../../../_definitions/IUseCase";

export type IUpdateProduct = IUseCase<IUpdateProductResponseDto,IUpdateProductRequestDto>