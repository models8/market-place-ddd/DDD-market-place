import {EventStoreDBClient} from "@eventstore/db-client";

const EVENTSTORE_HOSTNAME = process.env.EVENTSTORE_HOSTNAME
const url = `esdb://${EVENTSTORE_HOSTNAME}:21113?tls=false`

let EventStoreSales : EventStoreDBClient

const initSalesEventStore  = () => {
  try {
    EventStoreSales = EventStoreDBClient.connectionString(url)
    console.log("Connexion event store success")

  }catch (err:any) {
    console.log("Probleme de connexion a l'event store", err.message ? err.message : err)
  }
}

export {EventStoreSales, initSalesEventStore}
