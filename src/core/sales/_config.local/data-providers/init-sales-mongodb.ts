import mongoose, {Connection} from "mongoose";


export const mongo  = {

  port : process.env.MONGO_PORT ? +process.env.MONGO_PORT : 2017,
  hostname : process.env.MONGO_HOSTNAME || "NO_HOSTNAME",
  wrkDb : process.env.MONGO_DB || "NO_WRK_DB",
  user : process.env.MONGO_INITDB_USERNAME || "NO_USER",
  pwd : process.env.MONGO_INITDB_PASSWORD || "NO_PWD",
  authDb : process.env.MONGO_AUTH_DB || "NO_AUTH_DB",

}

const {user,pwd,wrkDb,hostname,port,authDb} = mongo

const url : string = `mongodb://${user}:${pwd}@${hostname}:${port}/${wrkDb}?authSource=${authDb}&retryWrites=true&w=majority`

const options = {
  useNewUrlParser : true,
  useUnifiedTopology : true,
  useCreateIndex : true ,
  useFindAndModify: false
}




const initSalesMongodb  = async () => {
  try {
    const connection = await mongoose.createConnection(url, options)
    console.log("Connexion mongodb success")
    MongoSalesConnection = connection

  }catch (err:any) {
    console.log("Probleme de connexion MongoDB : ", err.message ? err.message : err)
  }
}

let MongoSalesConnection : Connection

export {initSalesMongodb, MongoSalesConnection}