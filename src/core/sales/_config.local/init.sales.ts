import {initSalesEventStore} from "./data-providers/init-sales-event-store-db";
import {initSalesMongodb} from "./data-providers/init-sales-mongodb";
import {Router} from "express";
import {IRabbitmqBroker} from "../../../infrastructure/entry-points/RabbitmqBroker/IRabbitmqBroker";


export const init_sales = async (router: Router, broker: IRabbitmqBroker) => {

  await initSalesMongodb()

  initSalesEventStore()

  await import("./entry-points/init-sales-rabbitmq")
    .then(integrationEvents => integrationEvents.initSalesRabbitmq(broker))

  await import("./entry-points/sales-domain-event-dispatcher")
    .then(domainEvents => domainEvents.initSalesDomainEventListeners())

  await import("./entry-points/init-sales-express-router")
    .then(initexpress => initexpress.initSalesExpressRouter(router))

  console.log('---SALES INIT OK')
};


