import {Channel} from "amqplib";
import {IRabbitmqBroker} from "../../../../infrastructure/entry-points/RabbitmqBroker/IRabbitmqBroker";
import {SalesQueuesListeners} from "../../infra/entry-points/amqp-messages/QueuesListeners/SalesQueuesListeners";
import {IntegrationEventHandlersDI} from "../DI/IntegrationEventHandlersDI";

export let SalesBrokerChannel : Channel

export const initSalesRabbitmq =  async (broker : IRabbitmqBroker) => {
  SalesBrokerChannel = await broker.createChannel()
  const salesEventListeners = new SalesQueuesListeners(
    SalesBrokerChannel,
    IntegrationEventHandlersDI.OnCommandCancelledInstance,
    IntegrationEventHandlersDI.OnProductDeletedInstance,
  )
  await salesEventListeners.listen()
}

