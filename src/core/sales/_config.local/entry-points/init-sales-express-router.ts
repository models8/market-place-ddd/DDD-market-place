import {RoutersDI} from "../DI/RoutersDI";
import {Router} from "express";

export const initSalesExpressRouter = (router : Router) => {
  const cartRouter = RoutersDI.CartRouterInstance
  router.use(cartRouter.router)
}