import {EntityBuildersDI} from "./EntityBuildersDI";
import {ICartMapper} from "../../infra/data-providers/CartRepository/mapper/ICartMapper";
import {CartMapper} from "../../infra/data-providers/CartRepository/mapper/CartMapper";
import {ProductMapper} from "../../infra/data-providers/ProductRepository/mapper/ProductMapper";
import {IProductMapper} from "../../infra/data-providers/ProductRepository/mapper/IProductMapper";


export class MappersDI {

  static get CartMapperInstance(): ICartMapper {
    return new CartMapper(EntityBuildersDI.CartBuilderInstance)
  }

  static get ProductMapperInstance(): IProductMapper {
    return new ProductMapper(EntityBuildersDI.ProductBuilderInstance)
  }
  
}
