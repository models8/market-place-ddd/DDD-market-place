import {
  OnOrderCancelled
} from "../../infra/entry-points/amqp-messages/integration-events-handlers/OnOrderCancelled-Handler/OnOrderCancelled";
import {
  OnProductDeleted
} from "../../infra/entry-points/amqp-messages/integration-events-handlers/OnProductDeleted-Handler/OnProductDeleted";
import {UseCasesDI} from "./UseCasesDI";

export class IntegrationEventHandlersDI {


  static get OnProductDeletedInstance(): OnProductDeleted {
    return new OnProductDeleted(UseCasesDI.DeleteProductInstance)
  }

  static get OnCommandCancelledInstance(): OnOrderCancelled {
    return new OnOrderCancelled()
  }

}