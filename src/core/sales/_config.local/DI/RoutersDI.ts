import {CartRouter} from "../../infra/entry-points/http-rest/Cart/router/CartRouter";
import {ControllersDI} from "./ControllersDI";

export class RoutersDI {

  static get CartRouterInstance(): CartRouter {
    return new CartRouter(
      ControllersDI.AddProductToCartControllerInstance,
      ControllersDI.DisplayCartControllerInstance
    )
  }

}