import {IAuthenticationService} from "../../infra/data-providers/Services/IAuthenticationService";
import {AuthenticationService} from "../../infra/data-providers/Services/AuthenticationService";


export class ServicesDI {

  static get AuthServiceInstance(): IAuthenticationService {
    return new AuthenticationService()
  }

}
