import {
  AddProductToCartCT
} from "../../infra/entry-points/http-rest/Cart/controllers/AddProductToCartController/AddProductToCart-CT";
import {UseCasesDI} from "./UseCasesDI";
import {DisplayCartCT} from "../../infra/entry-points/http-rest/Cart/controllers/DisplayCartController/DisplayCart-CT";
import {PresentersDI} from "./PresentersDI";

export class ControllersDI {

  static get AddProductToCartControllerInstance(): AddProductToCartCT {
    return new AddProductToCartCT(
      UseCasesDI.AddProducttoCartInstance,
    );
  }

  static get DisplayCartControllerInstance(): DisplayCartCT {
    return new DisplayCartCT(
      UseCasesDI.DisplayCartInstance,
      PresentersDI.DisplayCartPresenterInstance
    )
  }
}
