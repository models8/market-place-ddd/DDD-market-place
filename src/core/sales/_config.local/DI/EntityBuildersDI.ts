import {ICartEntity, ICartBuildProps} from "../../domain/entities/Cart-Aggregate/ICartEntity";
import {CartBuilder} from "../../domain/entities/Cart-Aggregate/CartEntity";
import {IEntityBuilder} from "../../../../_definitions/EntityBuilder/IEntityBuilder";
import {ProductBuilder} from "../../domain/entities/Product-Aggregate/ProductEntity";
import {IProductEntity, IProductBuildProps} from "../../domain/entities/Product-Aggregate/IProductEntity";


export class EntityBuildersDI {

  static get CartBuilderInstance(): IEntityBuilder<ICartEntity, ICartBuildProps> {
    return new CartBuilder()
  }

  static get ProductBuilderInstance(): IEntityBuilder<IProductEntity, IProductBuildProps> {
    return new ProductBuilder()
  }

}
