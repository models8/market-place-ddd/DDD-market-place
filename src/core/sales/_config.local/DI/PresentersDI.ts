import {
  DisplayCartPresenter
} from "../../infra/entry-points/http-rest/Cart/presenters/DisplayCartPresenter/DisplayCart-Presenter";

export class PresentersDI {

  static get DisplayCartPresenterInstance(): DisplayCartPresenter {
    return new DisplayCartPresenter()
  }


}
