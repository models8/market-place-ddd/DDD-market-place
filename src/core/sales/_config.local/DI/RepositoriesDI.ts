import {ICartRepository} from "../../infra/data-providers/CartRepository/repo/ICartRepository";
import {CartRepository} from "../../infra/data-providers/CartRepository/repo/CartRepository";
import {MappersDI} from "./MappersDI";
import {ModelsDI} from "./ModelsDI";
import {ProductRepository} from "../../infra/data-providers/ProductRepository/repo/ProductRepository";
import {IProductRepository} from "../../infra/data-providers/ProductRepository/repo/IProductRepository";


export class RepositoriesDI {

  static get CartRepositoryInstance(): ICartRepository {
    return new CartRepository(
      ModelsDI.CartModelInstance,
      MappersDI.CartMapperInstance,
    )
  }

  static get ProductRepositoryInstance(): IProductRepository {
    return new ProductRepository(
      ModelsDI.ProductModelInstance,
      MappersDI.ProductMapperInstance,
    )
  }

}
