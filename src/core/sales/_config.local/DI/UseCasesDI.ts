import {RepositoriesDI} from "./RepositoriesDI";
import {EntityBuildersDI} from "./EntityBuildersDI";
import {AddProductToCart} from "../../application/use-cases/AddProductToCart-UC/AddProductToCart";
import {ServicesDI} from "./ServicesDI";
import {salesDomainEventDispatcher} from "../entry-points/sales-domain-event-dispatcher";
import {DisplayCart} from "../../application/use-cases/DisplayCart-UC/DisplayCart";
import {cartRM} from "../../infra/data-providers/ReadModels/CartRM/CartRM";
import {DeleteProduct} from "../../application/use-cases/DeleteProduct-UC/DeleteProduct";

export class UseCasesDI {

  static get AddProducttoCartInstance(): AddProductToCart {
    return new AddProductToCart(
      EntityBuildersDI.CartBuilderInstance,
      RepositoriesDI.CartRepositoryInstance,
      RepositoriesDI.ProductRepositoryInstance,
      ServicesDI.AuthServiceInstance,
      salesDomainEventDispatcher
    )
  }

  static get DisplayCartInstance(): DisplayCart {
    return new DisplayCart(
      cartRM,
      ServicesDI.AuthServiceInstance,
    )
  }

  static get DeleteProductInstance(): DeleteProduct {
    return new DeleteProduct(/*injections...*/)
  }


}
