import {init_sales} from "./core/sales/_config.local/init.sales";
import {expressAppInit} from "./_config.global/entry-points/expressjs-app-init";
import {rabbitmqBrokerInit} from "./_config.global/entry-points/rabbitmq-broker-init";


const run = async ()=>{

  const expressApp = expressAppInit()
  expressApp.middlewaresInit()

  const rabbimqBroker = await rabbitmqBrokerInit()
  await rabbimqBroker.initExchange()
  await init_sales(
    expressApp.router,
    rabbimqBroker
  )

  expressApp.routerInit()
  expressApp.boot()
}

run().then()


/*
const redisTest = async () => {
  const hostname =  process.env.REDIS_SNAPSHOT_HOSTNAME
  const redisDb = createClient({
    url:`redis://${hostname}:6379`
  });

  console.log(hostname)
  redisDb.on('error', (err) => console.log('Redis Client Error', err));
  await redisDb.connect();

  await redisDb.set('key', 'value');
  await redisDb.json.SET("serveur",".",{salut: "salutslaut"})

  const value = await redisDb.get('key');
  const valuejson = await redisDb.json.get('serveur');
}*/
