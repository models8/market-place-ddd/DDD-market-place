import ObjectID from "bson-objectid";
import {IEntity} from "../../src/_definitions/Entity/IEntity";


export interface I$pascalCase$EntityDto {
  _id : string
  productList : Array<[string, number]>
  cartOwner : string
  createdAt : Date
}

export interface I$pascalCase$BuildProps{
  _id? : ObjectID
}



export interface I$pascalCase$Entity extends IEntity{
  lean() : Required<I$pascalCase$EntityDto>
}

