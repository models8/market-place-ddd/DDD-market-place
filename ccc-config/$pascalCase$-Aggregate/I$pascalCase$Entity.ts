import ObjectID from "bson-objectid";
import {IAggregateRoot} from "../../src/_definitions/AggregateRoot/IAggregateRoot";

export interface I$pascalCase$EntityDto {
  _id : string
}

export interface I$pascalCase$BuildProps{
  _id? : ObjectID
}



export interface I$pascalCase$Entity extends IAggregateRoot{
  lean() : Required<I$pascalCase$EntityDto>
}

