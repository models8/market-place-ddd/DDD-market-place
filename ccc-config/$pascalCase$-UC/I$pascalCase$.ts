import {I$pascalCase$ResponseDto } from "./I$pascalCase$ResponseDto";
import {I$pascalCase$RequestDto } from "./I$pascalCase$RequestDto";
import {IUseCase} from "../../src/_definitions/IUseCase";

export type I$pascalCase$ = IUseCase<I$pascalCase$ResponseDto,I$pascalCase$RequestDto>